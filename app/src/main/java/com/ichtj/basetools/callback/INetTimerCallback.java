package com.ichtj.basetools.callback;

import com.ichtj.basetools.entity.NetBean;

public interface INetTimerCallback {
    void refreshNet(NetBean netBean);
}
