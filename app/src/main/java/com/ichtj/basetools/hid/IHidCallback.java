package com.ichtj.basetools.hid;

public interface IHidCallback {
    void receive(byte[] data);
    void connect(boolean connected);
}
