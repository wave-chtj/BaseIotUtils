package com.ichtj.basetools.touch;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.ichtj.basetools.R;
import com.ichtj.basetools.base.BaseActivity;

public class TouchDetectAty extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_touch);
    }
}
